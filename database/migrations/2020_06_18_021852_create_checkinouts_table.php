<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckinoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // `id` int(11) NOT NULL AUTO_INCREMENT,
        // `personnel_no` int(10) NOT NULL,
        // `checktime` datetime NOT NULL,
        // `checktype` varchar(1) NOT NULL,
        // `machinenumber` smallint(5) NOT NULL,

        Schema::create(env('CHECKINOUTS_TABLE', 'checkinouts'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('personnel_no');
            $table->datetime('checktime');
            $table->char('checktype', 1);
            $table->string('machinenumber');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkinouts');
    }
}

<?php

return [
    'default' => env('DB_CONNECTION_SFA'),
    'migrations' => 'migrations',
    'connections' => [
        'sfa' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_SFA'),
            'port' => env('DB_PORT_SFA'),
            'database' => env('DB_DATABASE_SFA'),
            'username' => env('DB_USERNAME_SFA'),
            'password' => env('DB_PASSWORD_SFA'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
        'eos' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST_EOS'),
            'port' => env('DB_PORT_EOS'),
            'database' => env('DB_DATABASE_EOS'),
            'username' => env('DB_USERNAME_EOS'),
            'password' => env('DB_PASSWORD_EOS'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ],
    ]
];
<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class FileDownloaded extends Event
{
    use SerializesModels;

    public $name, $extension;

    public function __construct($name, $extension)
    {
        $this->name = $name;
        $this->extension = $extension;
    }
}
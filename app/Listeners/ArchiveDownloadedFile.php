<?php

namespace App\Listeners;

use App\Events\FileDownloaded;
use App\Jobs\Job;
use ErrorException;
use Illuminate\Support\Facades\Log;

class ArchiveDownloadedFile extends Job
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FileDownloaded  $event
     * @return void
     */
    public function handle(FileDownloaded $event)
    {
        Log::info('event fired');

        $filesuffix =  DIRECTORY_SEPARATOR . $event->name . '.' . $event->extension;
        $source = env('DOWNLOAD_PATH', storage_path());
        $destination = env('ARCHIVE_PATH', storage_path());

        if (folder_exist($destination)) {
            if (!rename($source . $filesuffix, $destination . $filesuffix))
                throw new ErrorException('Could not archive file');

            Log::info($event->name . ' archived.');
        } else
            throw new ErrorException('Archive folder does not exists');
    }
}

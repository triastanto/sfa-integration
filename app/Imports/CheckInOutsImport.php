<?php

namespace App\Imports;

use App\CheckInOut;
use DateTime;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use PhpOffice\PhpSpreadsheet\Shared\Date;

HeadingRowFormatter::
    default('none');

class CheckInOutsImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            // available row heading:
            // Date, NIK, User, First Checkin, Last Checkout, Checkin Loc, Checkout Loc, Status,
            // Checkin Mock Locations, CheckOut Mock Locations, Checkin Device Id, Checkout Device Id

            // insert the check-in
            $this->insertIntoCheckInOut($row['Date'], $row['First Checkin'], $row['NIK'], $row['Checkin Loc'], 'I');
            // insert the check-out
            $this->insertIntoCheckInOut($row['Date'], $row['Last Checkout'], $row['NIK'], $row['Checkout Loc'], 'O');
        }
    }

    protected function insertIntoCheckInOut($date, $time, $personnel_no, $location, $checktype)
    {
        if ($date !== '' && $time !== '') {
            $checkin_date = Date::excelToDateTimeObject($date);
            $checkin_time = DateTime::createFromFormat("H:i a", $time);
            $checkin = $checkin_date->setTime(
                $checkin_time->format('H'),
                $checkin_time->format('i'),
                $checkin_time->format('s')
            );

            CheckInOut::firstOrCreate([
                'personnel_no' => $personnel_no,
                'checktime' => $checkin,
                'checktype' => $checktype,
                'machinenumber' => env('MACHINENUMBER', 900),
            ]);
        }
    }
}

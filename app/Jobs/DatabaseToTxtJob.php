<?php

namespace App\Jobs;

use App\CheckInOut;
use App\Upload;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class DatabaseToTxtJob extends Job
{
    protected $target, $upload;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Carbon $target, Upload $upload)
    {
        $this->target = $target;
        $this->upload = $upload;
    }

    protected function getTo() : Carbon
    {
        $upper = clone $this->target;
        $upper->hour = 9;
        $upper->minute = 0;
        return $upper;
    }

    protected function getFrom() : Carbon
    {
        $lower = clone $this->target;
        $lower->subDay();
        $lower->hour = 9;
        $lower->minute = 1;
        return $lower;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Absolute path to be used for text file
        $txt_path = join(DIRECTORY_SEPARATOR, [
            env('TXT_PATH', storage_path()),
            str_replace(
                'YYYYMMDD',
                $this->target->format('Ymd'),
                env('TXT_NAME', 'TimeEvent_SalesGO_YYYYMMDD')
            )
        ]) . '.txt';

        Log::info('Converting database records into text file '. $txt_path . ' starting...');

        // Delete previously generated text file exists on the same day
        if (file_exists($txt_path)) unlink($txt_path);

        // Iterate record from specified range from getFrom() and getTo()
        // and append every line to the new text file
        CheckInOut::where('machinenumber', env('MACHINENUMBER', 900))
            ->whereBetween('checktime', [
                $this->getFrom(),
                $this->getTo()
            ])
            ->get()
            ->each(function ($item) use ($txt_path) {
                // 20180806071100010618011159 OR 20180806 0711 00010618 01 1 159
                $line = sprintf("%s%s%'.08d%s%s%s",
                    $item->checktime->format('Ymd'),
                    $item->checktime->format('Hi'),
                    $item->personnel_no,
                    '01',
                    $item->numbered_checktype,
                    $item->machinenumber
                ) . PHP_EOL;
                $this->file_force_contents($txt_path, $line, FILE_APPEND);
            });

        // Update the history
        $this->upload->database_to_txt_at = Carbon::now();
        $this->upload->save();

        Log::info('Creating text file ' . $txt_path . ' has been generated.');
    }

    protected function file_force_contents( $fullPath, $contents, $flags = 0 ){
        $parts = explode( '/', $fullPath );
        array_pop( $parts );
        $dir = implode( '/', $parts );

        if( !is_dir( $dir ) )
            mkdir( $dir, 0777, true );

        file_put_contents( $fullPath, $contents, $flags );
    }
}

<?php

namespace App\Jobs;

use App\Imports\CheckInOutsImport;
use App\Upload;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class XlsxToDatabaseJob extends Job
{
    public $upload;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Upload $upload)
    {
        $this->upload = $upload;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path =  $this->upload->path . DIRECTORY_SEPARATOR . $this->upload->name;
        Log::info('Converting xlsx into database ' . $path  . ' starting...');
        Excel::import(new CheckInOutsImport, $path);
        Log::info('Archiving ' . $path  . ' has ended.');
    }
}

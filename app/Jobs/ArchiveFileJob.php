<?php

namespace App\Jobs;

use App\Download;
use Carbon\Carbon;
use ErrorException;
use Illuminate\Support\Facades\Log;

class ArchiveFileJob extends Job
{
    public $id, $name, $extension;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $name, $extension)
    {
        $this->id = $id;
        $this->name = $name;
        $this->extension = $extension;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Archiving ' . $this->name . '.' . $this->extension  . ' starting...');
        
        $filesuffix =  DIRECTORY_SEPARATOR . $this->name . '.' . $this->extension;
        $source_path = env('DOWNLOAD_PATH', storage_path());
        $destination_path = env('ARCHIVE_PATH', storage_path());
        $source = $source_path . $filesuffix;
        $destination = $destination_path . '_' . $this->id . $filesuffix;
        $destination = $destination_path . 
            DIRECTORY_SEPARATOR . 
            $this->name . '_' . 
            $this->id . '.' . 
            $this->extension;

        if (folder_exist($destination_path)) {
            if (!rename($source, $destination))
                throw new ErrorException('Could not archive file');
            
            $download = Download::find($this->id);
            $download->archive_path = env('ARCHIVE_PATH', storage_path());

            // this is just a workaroud because lumen can't read timezone linux!
            if (PHP_OS_FAMILY == 'Linux') {
                $download->created_at = Carbon::now()->subHour(7);
                $download->updated_at = Carbon::now()->subHour(7);
            } else {
                $download->created_at = Carbon::now();
                $download->updated_at = Carbon::now();
            }
            $download->save();
            
            Log::info($this->name . ' archived.');
        } else
            throw new ErrorException('Archive folder does not exists');

        Log::info('Archiving ' . $this->name . '.' . $this->extension  . ' has ended.');
    }
}

<?php

namespace App\Http\Middleware;

use App\Download;
use App\Jobs\ArchiveFileJob;
use Closure;
use Illuminate\Support\Facades\Log;

class AfterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $found = strpos($response, "200 OK");
        if ($found === false) {}
        else {
            $download = new Download();
            $download->name = $request->route('name');
            $download->extension = $request->route('extension');
            $download->download_path = env('DOWNLOAD_PATH', storage_path());
            
            if ($download->save()) {
                dispatch(new ArchiveFileJob($download->id, $download->name, $download->extension));
            }
        }

        return $response;
    }
}

<?php

namespace App\Http\Controllers;

use App\Jobs\DatabaseToTxtJob;
use App\Jobs\XlsxToDatabaseJob;
use App\Upload;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UploadController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'attachment' => [
                'required',
                'file',
                'mimetypes:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/octet-stream'
            ]
        ]);

        $today = Carbon::now();
        $upload_path = env('UPLOAD_PATH', storage_path());
        $filename = $request->file('attachment')->getClientOriginalName();
        $request->file('attachment')->move($upload_path, $filename);

        $upload = new Upload();
        $upload->name = $filename;
        $upload->path = $upload_path;
        $upload->xlsx_to_database_at = $today;

        if ($upload->save()) {
            $this->dispatch((new XlsxToDatabaseJob($upload))
                ->chain([new DatabaseToTxtJob($today, $upload)]));
        }

        return response()->json(['message' => 'File uploaded sucessfully.'], 201);
    }
}

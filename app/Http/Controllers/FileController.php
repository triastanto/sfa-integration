<?php

namespace App\Http\Controllers;

use ErrorException, Exception;
use Illuminate\Support\Facades\Log;

class FileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('archive');
    }

    public function download($name, $extension)
    {
        $filepath = env('DOWNLOAD_PATH', storage_path());

        try {
            return response()->download(
                $filepath . DIRECTORY_SEPARATOR . $name . '.' . $extension
            );
        } catch (Exception $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    // 'filepath' => $filepath, 'name' => $name, 'extension' => $extension
                ],
                404
            );
        }
    }

    public function archive($name, $extension)
    {
        $filesuffix =  DIRECTORY_SEPARATOR . $name . '.' . $extension;
        $source = env('DOWNLOAD_PATH', storage_path());
        $destination = env('ARCHIVE_PATH', storage_path());

        try {
            if (folder_exist($destination)) {
                if (!rename($source . $filesuffix, $destination . $filesuffix))
                    throw new ErrorException('Could not archive file');

                return response()->json(
                    [
                        'message' => 'file archived!',
                        // 'source' => $source, 'destination' => $destination
                    ],
                    200
                );
            } else
                throw new ErrorException('Archive folder does not exists');
        } catch (ErrorException $e) {
            return response()->json(
                [
                    'message' => $e->getMessage(),
                    // 'source' => $source, 'destination' => $destination
                ],
                202
            );
        }
    }
}

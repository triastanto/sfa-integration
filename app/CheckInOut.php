<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckInOut extends Model
{
    protected $fillable = [
        'personnel_no',
        'checktime',
        'checktype',
        'machinenumber',
        'location',
    ];

    protected $dates = [
        'checktime'
    ];

    public $timestamps = false;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setConnection(env('DB_CONNECTION_EOS'));
        $this->setTable(env('CHECKINOUTS_TABLE'));
    }

    public function getNumberedChecktypeAttribute()
    {
        if ($this->checktype == 'O') return 0;
        else if ($this->checktype == 'I') return 1;
    }
}